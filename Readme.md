# Rat

`cat` - Now with more rainbows.

![screenshot](screenshot.png)

Inspired by [lolcat](https://github.com/busyloop/lolcat) but written in rust and supports continuous streams like /dev/urandom
